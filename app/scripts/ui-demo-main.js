/**
 * 
 * Dismiss Controller -
 *
 * Markup: 
 *     .js-dismiss          : Class of controlling element. Must be child of 
 *         a bootstrap column ([class^="col-"]). Hides element.       
 *     .js-show-dismissed   : Class to show all dismissed elements.
 *
 * 
 * Toggle Controller -
 *  
 * Markup: 
 *     .js-toggle                       : Class of controlling element   
 *     .js-toggle-target (optional)     : Must be immediate child element 
 *         (can be overridden with data-toggle-target)
 *     data-toggle-target (optional)    : jQuery selector for target element(s)
 *     data-toggle-event (optional)     : additional event triggers (separated 
 *         by space) - accepts mouseenter, mouseleave
 *     data-toggle-group (optional)     : Elements in a toggle-group that 
 *         aren't the event target will be deactivated when one of their 
 *         group memebers is activated
 *     data-toggle-override (optional)  : When set to true, controller won't 
 *         run jQuery's slidetoggle method on target. Target elements won't be 
 *         set to display: none at init.
 *     data-toggle-reverse              : When set to true, data won't be hidden
 *         at start.
 *
 * Options: (Override default selectors)
 *     defaultSelector  : CSS selector of toggle trigger element
 *     defaultTarget    : CSS selector - element must be 
 *         immediate child of toggle trigger. Otherwise, 
 *         use data-toggle-target to decouple.
 *         
 * Behavior: 
 *     click        :
 *         toggles .is-pressed on .js-toggle
 *         toggles .is-active-toggle on target element
 *     mouseenter   :
 *         adds class is-hovered from .js-toggle
 *         adds .is-active-toggle on target element
 *     moouseleave  :
 *         removes class is-hovered from .js-toggle
 *         removes .is-active-toggle on target element (unless click event is active)
 *
 * Appearances for state changes must be declared in CSS.
 */


(function($){
    'use strict';
    var uiDemoController = uiDemoController || {};
    
    uiDemoController.hoverWatch = (function() { 
        
        var bindHover = function(selector) { 
                $(selector).on('mouseover', function(e){
                    e.stopPropagation();
                    $(this).addClass('is-hovered');
                });

                $(selector).on('mouseout', function(e){
                    e.stopPropagation();
                    $(this).removeClass('is-hovered');
                });
            },
            hoverInit = function(userConfig) { 
                
                if (userConfig) { 
                        var i = 0;
                        while (userConfig[i]) { 
                            bindHover(userConfig[i]);
                            i++;
                        }

                } else { 
                    return; 
                }              
            };

            return {
                init: hoverInit
            };
    })();

    uiDemoController.dismiss = (function() {
        var configDefaults = {
                defaultDismissSelector: '.js-dismiss',
                defaultShowDismissedSelector: '.js-show-dismissed'
            },

            dismissSelector, showDismissedSelector = '',

            showDismissed = function() { 
                $('.is-dismissed').fadeIn().removeClass('is-dismissed');
            },

            setConfig = function(userConfig) { 
                if (userConfig) { 
                    dismissSelector = userConfig.dismissSelector ? userConfig.dismissSelector : configDefaults.defaultDismissSelector;
                    showDismissedSelector = userConfig.showDismissedSelector ? userConfig.showDismissedSelector : configDefaults.defaultShowAllSelector;
                } else { 
                    dismissSelector = configDefaults.defaultDismissSelector;
                    showDismissedSelector = configDefaults.defaultShowDismissedSelector;
                }
            },

            dismissAction = function($el) {
                $el.parents('[class^=col-]').fadeToggle().addClass('is-dismissed');
            },

            bindEvent = function() {
                
                $(dismissSelector).on('click', function(e) {     
                    dismissAction($(this));
                    e.stopPropagation();
                });

                $(showDismissedSelector).on('click', function() {                       
                    showDismissed();
                });
            },

            dismissInit = function(userConfig) {
                setConfig(userConfig);
                bindEvent();

            };

            return { 
                init: dismissInit,
                showAll: showDismissed
            };

    })();

    uiDemoController.toggle = (function() {
        var configDefaults = { 
                defaultSelector: '.js-toggle',
                defaultTarget: '.js-toggle-target'
            },

            toggleSelector, toggleTarget = '',

            setConfig = function(userConfig) {
                if (userConfig) { 
                    toggleSelector = userConfig.toggleSelector ? userConfig.toggleSelector : configDefaults.defaultSelector;
                    toggleTarget = userConfig.toggleTarget ? userConfig.toggleTarget : configDefaults.defaultTarget;
                } else {
                    toggleSelector = configDefaults.defaultSelector;
                    toggleTarget = configDefaults.defaultTarget;
                }
            },

            getData = function($el) { 
                //Return object data
                return $el.data();
            },

            acquireTarget = function(target, $el) {
                //Determine whether element has a target, 
                //if not use default selector
                return target ? $(target) : $el.children(toggleTarget);
            },

            isOverride = function(data) { 
                //Determine whether element has behavior override
                return data.toggleOverride === true ? true : false;
            },

            isGroupMember = function(data) { 
                //Determine whether element is member of toggle group
                return data.toggleGroup ? true : false;
            },

            isTargetActive = function(target) { 
                //Determine whether element is currently active
             
                    return target.hasClass('is-active-toggle') ? true : false;
             
            },

            targetDetermineAction   = function($el) { 
                var data            = getData($el),
                    
                    target          = acquireTarget(data.toggleTarget, $el),
                    targetIsActive  = isTargetActive(target),
                    override        = isOverride(data),
                    hasGroup        = isGroupMember(data),
                    groupID         = data.toggleGroup;
                    
                    if (hasGroup) { 
                        toggleGroup($el, groupID, override);
                    }

                    if (targetIsActive) {
                        targetDeactivate(target, override);
                        closeChildren($el, override); 
                    } else {
                        targetActivate(target, override); 
                    }

            },

            targetActivate = function(target, override) {
                if ( !override ) { 
                    target.slideToggle(200);
                }
                target.addClass('is-active-toggle');                
            },

            targetDeactivate = function(target, override) { 
                if ( !override ) { 
                    target.slideUp(200);
                }
                target.removeClass('is-active-toggle');
            },

            closeChildren = function($el, override){
                $el.find('.is-active-toggle, .is-pressed').removeClass('is-active-toggle is-pressed');
                if (!override) { 
                    $el.find(toggleTarget).slideUp(200); 
                }
            },

            toggleGroup = function($el, group, override) { 
                //Iterate through all members of toggle group               
                $('[data-toggle-group="' + group + '"]').each(function(){
                    var $this = $(this);
                    //If current item is the event target, return
                    if ( $el.is($this) ) {                         
                        return;
                    } else { 
                    //Otherwise, remove active class, and deactivate target
                        var data            = getData($this),
                            target          = acquireTarget(data.toggleTarget, $this);
                        
                        $this.removeClass('is-pressed');
                        closeChildren($this);
                        targetDeactivate(target, override);
                    }
                });
            },

            /*eventMouseEnter = function($el) { 
                //Trigger toggle action
                //if not in click state
                if (!$el.hasClass('is-pressed')) { 
                    targetDetermineAction($el);
                }
                $el.addClass('is-hovered');
            },

            eventMouseLeave = function($el) { 
                //Trigger toggle action 
                //if not in click state
                if (!$el.hasClass('is-pressed')) { 
                    targetDetermineAction($el);
                }
                $el.removeClass('is-hovered');

            },*/

            eventClick = function($el) { 
                targetDetermineAction($el);
                $el.toggleClass('is-pressed');
            },

            bindClick = function() {
                $(toggleSelector).on('click', function(e) {
                    e.stopPropagation();
                    eventClick($(this));
                });
            },

            /*bindSpecial = function(event, $el) { 
                switch(event) {
                    case 'mouseenter':
                        $el.on('mouseenter', function(e) {
                            e.stopPropagation();
                            eventMouseEnter($el);                            
                        });
                    break;
                    case 'mouseleave':
                        $el.on('mouseleave', function(e) {
                            e.stopPropagation(); 
                            eventMouseLeave($el); 
                        });
                    break;
                }
            },*/

            toggleInit = function(userConfig) { 
                setConfig(userConfig);
                bindClick();           
                $(toggleSelector).each(function(){ 
                    var $this       = $(this),
                        data        = getData($this),
                        isReverse   = data.toggleReverse === true ? true : false,
                        target      = acquireTarget(data.toggleTarget, $this);

                        //Hide the targets by default unless they have the is-active-toggle class, 
                        //or have toggle behavior overridden.
                        if (isReverse) { 
                            target.addClass('is-active-toggle');
                        }

                        if( !target.hasClass('is-active-toggle') && data.toggleOverride !== true ) { 
                            target.hide();
                        } 
                        //Set up special case event bindings
                        /*if (data.toggleEvent) { 
                            var events = data.toggleEvent.split(' '),
                                i = 0;

                                while (events[i]) { 
                                    bindSpecial(events[i], $this);
                                    i++;
                                }

                        } else { 
                            bindSpecial('click', $this);                            
                        }*/
                });
            };

            return { 
                init: toggleInit,
                setConfig: setConfig
            };
    })();

    $(document).ready(function(){
        uiDemoController.hoverWatch.init(['.nav-block-item', '.db-cont-toolbar-icon']);
        uiDemoController.dismiss.init();
        uiDemoController.toggle.init();
    });

})(jQuery);